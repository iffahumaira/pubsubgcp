package th.co.ktb.next.archetype.service;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.interceptor.PubInterceptor;
import th.co.ktb.next.archetype.model.request.RequestMessage;
import th.co.ktb.next.archetype.model.response.ResponseMessage;
import th.co.ktb.next.common.service.BaseService;

@Service
public class PubInterceptorBaseService implements BaseService<RequestMessage, ResponseMessage> {

    private Adaptor adaptor;

    public PubInterceptorBaseService(Adaptor adaptor) {
        this.adaptor = adaptor;
    }

    @Override
    public ResponseMessage execute(RequestMessage requestMessage) {

        RequestMessage input = RequestMessage.builder()
                .message(requestMessage.getMessage())
                .topic(requestMessage.getTopic())
                .build();

        PubInterceptor pubInterceptor = new PubInterceptor();

        Message<RequestMessage> msg = MessageBuilder.withPayload(input)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build();

        return adaptor.pubInterceptor(input);
    }
}
