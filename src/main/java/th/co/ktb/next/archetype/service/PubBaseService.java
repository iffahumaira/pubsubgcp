package th.co.ktb.next.archetype.service;

import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.model.request.RequestMessage;
import th.co.ktb.next.archetype.model.response.ResponseMessage;
import th.co.ktb.next.common.service.BaseService;

@Service
public class PubBaseService implements BaseService<RequestMessage, ResponseMessage> {

    private Adaptor adaptor;

    public PubBaseService(Adaptor adaptor) {
        this.adaptor = adaptor;
    }

    @Override
    public ResponseMessage execute(RequestMessage requestMessage) {
        return adaptor.sendToPublish(requestMessage);
    }
}
