package th.co.ktb.next.archetype.adaptor;

import th.co.ktb.next.archetype.model.request.RequestMessage;
import th.co.ktb.next.archetype.model.response.ResponseMessage;

public interface Adaptor {

    ResponseMessage sendToPublish(RequestMessage requestMessage);
    ResponseMessage pubInterceptor(RequestMessage requestMessage);
}
