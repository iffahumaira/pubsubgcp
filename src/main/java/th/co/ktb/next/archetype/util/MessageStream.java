package th.co.ktb.next.archetype.util;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/* FOR PUBLISHER INTERCEPTOR*/

public interface MessageStream {

    String OUTPUT = "outbound";

    @Output(OUTPUT)
    MessageChannel pubGcpInterceptor();
}
