package th.co.ktb.next.archetype.model.entity;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Message {

    private String message;
    private String topic;

}
